<?php
/**
 *
 */

require_once 'rdg/FotoGateway.php';

$f1 = new FotoGateway();
$f1->file = $_FILES['img'];
$f1->timestamp = date("Y-m-d H:m:s", time());

$rmspace = explode(" ", $f1->timestamp);
$time = explode("-", $rmspace[0]);

$path = "media/{$time[0]}/{$time[1]}/{$time[2]}/";

if (!empty($f1->file['name'])){

    if (!is_dir($path)){
        mkdir($path, 0777, true);
    }

    if (move_uploaded_file($f1->file['tmp_name'], $path . $f1->file['name']))
    {
        if ($f1->save()) {
            echo "Upload realizado com sucesso. <br /> <a href='index.php'>Voltar para o Página inicial</a>";
        } else {
            echo "Falha ao realizar o upload!";
        }
    }
} else {
    echo "Nada enviado. <br /> <a href='index.php'>Voltar para o Página inicial</a>";
}