<?php
/**
 *
 */

require_once 'rdg/FotoGateway.php';

$f1 = new FotoGateway();

$foto = $f1->find($_GET['id']);

?>

<html>
    <head></head>
    <body>
        <h1>Row Data Gateway</h1>
        <h2>Design Pattern</h2>
        <h3>Study purpose only</h3>
        <p>Editar nome da imagem:</p>
        <form action="updaterdg.php" method="POST">
            <input type="text" name="nome" value="<?php print $foto->nome; ?>" />
            <input type="hidden" name="id" value="<?php print $foto->id; ?>" />
            <input type="submit" />
        </form>
        <a href="index.php">Voltar à página inicial</a>
    </body>
</html>
