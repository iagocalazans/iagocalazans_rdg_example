<?php
/**
 *
 */

require_once 'rdg/FotoGateway.php';

$foto = new FotoGateway();
?>

<html>
    <head>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    </head>
    <body>
        <h1>Row Data Gateway</h1>
        <h2>Design Pattern</h2>
        <form method="POST" action="uploadrdg.php" enctype="multipart/form-data">
            <input type="file" name="img" />
            <input type="submit" name="submit" value="Upload!">
        </form>
        <h3>Study purpose only</h3>
        <?php if (null == $foto->all()): ?>
            Não existem imagens.
        <?php else: ?>
            <table>
                <tr>
                    <th>Image</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
            <?php foreach ($foto->all() as $value): ?>
                <tr>
                    <td style="text-align: center;" width="125px"><img src="<?php echo $value->path.$value->nome; ?>" width="100px" /></td>
                    <td width="200px"><?php echo $value->nome; ?></td>
                    <td style="text-align: center;"><a  target="_blank" href="<?php echo $value->path.$value->nome; ?>"><i class="fa fa-search" aria-hidden="true"></i></a> <a href="editrdg.php?id=<?php echo $value->id; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>  <a href="deleterdg.php?id=<?php echo $value->id; ?>"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </table>
        <?php endif; ?>
    </body>
</html>
