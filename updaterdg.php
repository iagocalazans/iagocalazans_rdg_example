<?php
/**
 *
 */

require_once 'rdg/FotoGateway.php';

$f1 = new FotoGateway();

$foto = $f1->find($_POST['id']);

$path = $foto->path.$foto->nome;

$renamePath = $foto->path.$_POST['nome'];
rename($path, $renamePath);

$foto->nome = $_POST['nome'];

if ($foto->save()){
    echo 'Nome alterado com sucesso. <br /> <a href=\'index.php\'>Voltar para o Página inicial</a>';
} else {
    echo 'Ops, algo inesperado aconteceu';
}
