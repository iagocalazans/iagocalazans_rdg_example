<?php
/**
 *
 */

require_once 'rdg/FotoGateway.php';

$f1 = new FotoGateway();
$foto = $f1->find($_GET['id']);

try {
    if ($foto->delete())
    {
        echo 'Deletado com sucesso. <br /> <a href=\'index.php\'>Voltar para o Página inicial</a>';
    }
} catch (Exception $e) {
    print $e->getMessage();
}