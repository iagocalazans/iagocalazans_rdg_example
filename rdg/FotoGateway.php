<?php

/**
 * @author Iago Calazans
 */
class FotoGateway
{
    /**
     *
     */
    public function __construct()
    {
        try {
            $conn = new PDO('sqlite:db/database.sqlite');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::setConnection($conn);
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

    /**
     * @var PDO
     */
    private static $conn;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @param void $prop
     * @return $this->data
     */
    public function __get($prop)
    {
        return $this->data[$prop];
    }

    /**
     * @param void $prop
     * @param void $value
     */
    public function __set($prop, $value)
    {
        $this->data[$prop] = $value;
    }

    /**
     * @param PDO $conn
     */
    public static function setConnection(PDO $conn)
    {
        self::$conn = $conn;
    }

    public function find($id)
    {
        $sql = 'SELECT * FROM fotos WHERE id = '.$id;

        $result = self::$conn->query($sql);
        return $result->fetchObject(__CLASS__);
    }

    /**
     * @param $filter
     * @return FotoGateway
     */
    public function all($filter = null)
    {
        if (!is_null($filter)){
            $sql = "SELECT * FROM fotos WHERE {$filter}";
        } else {
            $sql = "SELECT * FROM fotos";
        }

        $result = self::$conn->query($sql);
        return $result->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     *
     */
    public function save()
    {
        if (empty($this->data['id'])){
            $id = $this->getLastId() + 1;
            $sql = "INSERT INTO fotos (id, nome, path, create_date) VALUES ('{$id}', '{$this->data['file']['name']}', '{$this->setPath()}', '{$this->data['timestamp']}')";


        } else {
            $sql = "UPDATE fotos SET nome = '{$this->data['nome']}' WHERE id = '{$this->data['id']}'";
        }

        try{
            return self::$conn->exec($sql);
        } catch (Exception $e){
            print $e->getMessage();
        }

    }

    /**
     *
     */
    public function delete()
    {
        unlink($this->getPath() . $this->data['nome']);

        $sql = "DELETE FROM fotos WHERE id = {$this->data['id']}";
        return self::$conn->query($sql);
    }

    /**
     *
     */
    public function getLastId()
    {
        $sql = "SELECT max(id) as max FROM fotos";
        $result = self::$conn->query($sql);
        $data = $result->fetch(PDO::FETCH_OBJ);
        return $data->max;
    }

    public function setPath()
    {
        $rmspace = explode(" ", $this->data['timestamp']);
        $time = explode("-", $rmspace[0]);

        $path = "media/{$time[0]}/{$time[1]}/{$time[2]}/";

        return $path;
    }

    public function getPath()
    {
        $rmspace = explode(" ", $this->data['create_date']);
        $time = explode("-", $rmspace[0]);
        $path = "media/{$time[0]}/{$time[1]}/{$time[2]}/";

        return $path;
    }
}
